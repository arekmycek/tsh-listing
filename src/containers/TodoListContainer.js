import { connect } from 'react-redux';
import { addTodo, deleteTodo, editTodo, changeStatus, filterStatus, sortTodos } from '../actions/todos';
import TodoList from '../components/TodoList';

function sortByName(a, b, asc) {
    let todo1 = a.text.toLowerCase(),
        todo2 = b.text.toLowerCase();

    if(asc) {
        return todo1 < todo2 ? -1 : (todo1 > todo2) ? 1 : 0;
    } else {
        return todo1 > todo2 ? -1 : (todo1 < todo2) ? 1 : 0;
    }
}

function getSortedTodos(todos, sort, filter) {
    let filteredTodos = getFilteredTodos(todos, filter);
    switch(sort) {
        case 'SORT_BY_NAME_ASC':
            return filteredTodos.sort((a, b) => sortByName(a, b, true));
        case 'SORT_BY_NAME_DESC':
            return filteredTodos.sort((a, b) => sortByName(a, b, false));
        case 'SORT_BY_STATUS_DONE':
            return filteredTodos.sort((a, b) => (a.done && !b.done) ? -1 : 1);
        case 'SORT_BY_STATUS_TODO':
            return filteredTodos.sort((a, b) => (a.done && !b.done) ? 1 : -1);
        default:
            return filteredTodos;
    }
}

function getFilteredTodos(todos, filter) {
    switch (filter) {
        case 'HIDE_DONE':
            return todos.filter(todo => !todo.done);
        default:
            return todos;
    }
}

function mapStateToProps(state) {
    return {
        todos: getSortedTodos(state.todos.list, state.todos.sort, state.todos.filter),
        filter: state.todos.filter,
        sort: state.todos.sort
    };
}

function actions(dispatch) {
    return {
        addTodo: (text) => dispatch(addTodo(text)),
        deleteTodo: (id) => dispatch(deleteTodo(id)),
        editTodo: (id, text) => dispatch(editTodo(id, text)),
        changeStatus: (id) => dispatch(changeStatus(id)),
        filterStatus: (filter) => dispatch(filterStatus(filter)),
        sortTodos: (param) => dispatch(sortTodos(param))
    };
}

const TodoListContainer = connect(mapStateToProps, actions)(TodoList);
export default TodoListContainer;
