const address = "http://test-api.kuria.tshdev.io/";

export function getPayments() {
    return (dispatch, getState) => {
        return fetchPayments(buildUrl(getState().listing.searchParams))
            .then((data) => {
                dispatch({ type: 'GET_PAYMENTS', data });
            })
            .catch(() => {
                dispatch({ type: 'GET_PAYMENTS', data: {pagination: {}, payments: []} });
            });
    }
}

export function changeSearchParams(params) {
    return { type: 'CHANGE_SEARCH_PARAMS', params};
}

export function changePage(link) {
    return dispatch => {
        return fetchPayments(address+'?'+link)
            .then((data) => {
                dispatch({ type: 'GET_PAYMENTS', data });
            })
            .catch(() => {
                dispatch({ type: 'GET_PAYMENTS', data: {pagination: {}, payments: []} });
            });
    }
}

function fetchPayments(url) {
    return fetch(url)
        .then(res => res.json());
}

function buildUrl(params) {
    const { query, rating, page } = params;

    let string = address + '?';
    if(query.length > 0) {
        string += `query=${query}`;
    }
    if(rating.length > 0) {
        string += `&rating=${rating}`;
    }
    if(page.length > 0) {
        string += `&page=${page}`;
    }
    return string;
}
