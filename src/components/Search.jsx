import React, { Component } from 'react';
import '../styles/search.css';
import { connect } from 'react-redux';
import { changeSearchParams, getPayments } from '../actions/payments';

class SearchBlock extends Component {
    constructor() {
        super();
        this.state = {
            query: '',
            rating: '0'
        }
    }

    handleChange(name) {
        return (e) => {
            this.props.changeSearchParams({[name]: e.target.value});
            this.setState({[name]: e.target.value});
        }
    }

    reset() {
        this.setState({query: '', rating: '0'});
        this.props.changeSearchParams({ query: '', rating: ''});
    }

    render() {
        return (
            <div className="searchBlockContainer row">
                <div className="searchBlock col-md-10 col-md-offset-1 col-sm-12">
                    <div className="form-group col-sm-6 col-xs-12">
                        <input type="text" placeholder="Search suppliers" value={this.state.query}
                        onChange={this.handleChange('query')} className="searchInput"/>
                    </div>
                    <div className="form-group col-sm-3 col-xs-6">
                        <div className="select-container">
                            <select onChange={this.handleChange('rating')} value={this.state.rating}>
                                <option value="0" disabled>Select pound rating</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                            </select>
                        </div>
                    </div>
                    <div className="form-group col-sm-3 col-xs-12">
                        <input type="button" value="Reset" onClick={this.reset.bind(this)} className="button resetButton"/>
                        <input type="button" value="Search" onClick={this.props.getPayments} className="button searchButton"/>
                    </div>
                </div>
            </div>
        );
    }
}


function actions(dispatch) {
    return {
        changeSearchParams: (params) => dispatch(changeSearchParams(params)),
        getPayments: () => dispatch(getPayments())
    };
}

const Search = connect(null, actions)(SearchBlock);
export default Search;
