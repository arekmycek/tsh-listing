import React, { Component } from 'react';
import '../styles/payment.css';
import pound from '../images/pound.png';
import poundActive from '../images/poundActive.png';

class PaymentRow extends Component {
    generatePounds(activeAmount) {
        let pounds = [];
        for( let i = 0; i < 5; i++) {
            if(i < activeAmount) {
                pounds.push(<img src={poundActive} className="pound" alt="pound" key={i}/>);
            } else {
                pounds.push(<img src={pound} className="pound" alt="pound" key={i}/>);
            }
        }
        return pounds;
    }

    render() {
        const data = this.props.data;
        return (
            <tr onClick={this.props.handleModal}>
                <td>{data.payment_supplier}</td>
                <td>{this.generatePounds(data.payment_cost_rating)}</td>
                <td>{data.payment_ref}</td>
                <td>£{data.payment_amount}</td>
            </tr>
        );
    }
}

export default PaymentRow;
