import React, { Component } from 'react';
import { connect } from 'react-redux';
import PaymentRow from './PaymentRow';
import '../styles/payment.css';
import { getPayments } from '../actions/payments';
import { Modal } from 'react-bootstrap';

class Payments extends Component {
    constructor() {
        super();
        this.state = {
            modalOpened: false,
            payment: {}
        }
    }

    componentWillMount() {
      this.props.getPayments();
    }

    handleModal(data) {
        if(!this.state.modalOpened) {
            this.setState({payment: data});
        }
        this.setState({modalOpened: !this.state.modalOpened});
    }

    render() {
        return (
            <div className="row">
                <div className="table-responsive">
                    <table className="table table-striped payments-table">
                        <thead>
                            <tr>
                                <th className="col-xs-7">Supplier</th>
                                <th className="col-xs-3">Pound Rating</th>
                                <th className="col-xs-1">Reference</th>
                                <th className="col-xs-1">Value</th>
                            </tr>
                        </thead>
                        <tbody>
                            {this.props.listing.map((row, i) => {
                                return (
                                    <PaymentRow data={row} key={i} handleModal={() => this.handleModal(row)}/>
                                );
                            })}
                        </tbody>
                    </table>
                </div>
                <Modal show={this.state.modalOpened} onHide={this.handleModal.bind(this)}>
                    <Modal.Header closeButton>
                        <Modal.Title>Payment</Modal.Title>
                    </Modal.Header>
                    <Modal.Body className="table-responsive">
                        <table className="table table-striped payments-table">
                            <thead>
                                <tr>
                                    <th className="col-xs-6">Supplier</th>
                                    <th className="col-xs-4">Pound Rating</th>
                                    <th className="col-xs-1">Reference</th>
                                    <th className="col-xs-1">Value</th>
                                </tr>
                            </thead>
                            <tbody>
                                <PaymentRow data={this.state.payment}  handleModal={() => {}}/>
                            </tbody>
                        </table>
                    </Modal.Body>
                </Modal>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        listing: state.listing.payments,
    };
}

function actions(dispatch) {
    return {
        getPayments: () => dispatch(getPayments())
    };
}

const PaymentsList = connect(mapStateToProps, actions)(Payments);
export default PaymentsList;
