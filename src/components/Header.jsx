import React, { Component } from 'react';
import '../styles/header.css';

class Header extends Component {
    render() {
        return (
            <header>
                <span>Where your money goes</span>
                <p>Payments made by Chichester District Council to invidual suppliers with a value over £500 made within October.</p>
                <hr/>
            </header>
        );
    }
}

export default Header;
