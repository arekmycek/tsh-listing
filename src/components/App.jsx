import React, { Component } from 'react';
import Header from './Header';
import Search from './Search';
import PaymentsList from './PaymentsList';
import Pagination from './Pagination';
import '../styles/index.css';

class App extends Component {
    render() {
        return (
            <div className="container">
                <Header />
                <Search />
                <PaymentsList />
                <Pagination />
            </div>
        );
    }
}

export default App;
