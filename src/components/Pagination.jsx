import React, { Component } from 'react';
import '../styles/pagination.css';
import { connect } from 'react-redux';
import { changePage, getPayments } from '../actions/payments';

class PaginationList extends Component {
    previousPage() {
        const pag = this.props.pagination;
        if(pag.left) {
            this.props.changePage(pag.links[parseInt(pag.current, 10)-1]);
        }
    }

    nextPage() {
        const pag = this.props.pagination;
        if(pag.right) {
            this.props.changePage(pag.links[parseInt(pag.current, 10)+1]);
        }
    }

    render() {
      const links = this.props.pagination.links;
      return (
          <nav aria-label="Page navigation">
              <ul className="pagination center">
                  <li>
                      <a href="#" onClick={this.previousPage.bind(this)} className="pagination-link">
                          <span>&lang;</span>
                      </a>
                  </li>
                  {Object.keys(links).map((data, i) => {
                      return (
                          <li key={i}><a href="#" onClick={() => this.props.changePage(links[data])}
                          className={this.props.pagination.current === data? "pagination-link pagination-link-active" : "pagination-link"}>{data}</a></li>
                      );
                  })}
                  <li>
                      <a href="#" aria-label="Next" onClick={this.nextPage.bind(this)} className="pagination-link">
                          <span aria-hidden="true">&rang;</span>
                      </a>
                  </li>
              </ul>
          </nav>
        );
    }
}

function mapStateToProps(state) {
    return {
        pagination: state.listing.pagination,
    };
}

function actions(dispatch) {
    return {
        changePage: (params) => dispatch(changePage(params)),
        getPayments: () => dispatch(getPayments())
    };
}

const Pagination = connect(mapStateToProps, actions)(PaginationList);
export default Pagination;
