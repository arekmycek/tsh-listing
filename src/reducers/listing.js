const initialState = {
    payments: [],
    pagination: {links: {}},
    searchParams: {
        query: '',
        rating: '',
        page: ''
    }
};

function listing(state = initialState, action) {
    switch (action.type) {
        case 'GET_PAYMENTS':
            return { ...state, ...action.data };
        case 'CHANGE_SEARCH_PARAMS':
            return { ...state, searchParams: {...state.searchParams, ...action.params}};
        default:
            return state;
        }
}

export default listing;
