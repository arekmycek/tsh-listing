import { combineReducers } from 'redux';
import listing from './listing';

const reducers = combineReducers({ listing });

export default reducers;
